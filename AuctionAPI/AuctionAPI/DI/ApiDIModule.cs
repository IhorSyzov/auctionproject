﻿using AuctionBLL;
using AuctionBLL.Interfaces;
using AuctionBLL.Services;
using AutoMapper;
using Ninject;
using Ninject.Modules;
using Ninject.Web.WebApi.Filter;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Validation;

namespace AuctionAPI.DI
{
    public class ApiDIModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IBetService>().To<BetService>();
            Bind<ILotService>().To<LotService>();
            Bind<IUserService>().To<UserService>();
            //Mapper
            Bind<DefaultFilterProviders>().ToConstant(new DefaultFilterProviders(GlobalConfiguration.Configuration.Services.GetFilterProviders()));
            var mapperConfiguration = CreateConfiguration();
            Bind<MapperConfiguration>().ToConstant(mapperConfiguration).InSingletonScope();
            Bind<IMapper>().ToMethod(ctx =>
            new Mapper(mapperConfiguration, type => ctx.Kernel.Get(type)));
            //
            Bind<DefaultModelValidatorProviders>().ToConstant(new DefaultModelValidatorProviders(GlobalConfiguration.Configuration.Services.GetServices(typeof(ModelValidatorProvider)).Cast<ModelValidatorProvider>()));
        }

        private MapperConfiguration CreateConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                // Add all profiles in current assembly
                cfg.AddProfile(new ApiMapperProfile());
                cfg.AddProfile(new MapperProfile());
            });

            return config;
        }
    }
}