﻿using AuctionDAL.Entities;
using System.Collections.Generic;
using System.Data.Entity;

namespace AuctionDAL.EF
{

    public class CreateInitializer : DropCreateDatabaseAlways<MainContext>
    {
        protected override void Seed(MainContext db)
        {
            Lot lot1 = new Lot() { Category = "Clothes", Name = "Lot1", Info = "Info lot1", StartPrice = 200, Duration = 12 };
            Lot lot2 = new Lot() { Category = "Clothes", Name = "Lot2", Info = "Info lot2", StartPrice = 140, Duration = 6 };
            Lot lot3 = new Lot() { Category = "Toys", Name = "Lot3", Info = "Info lot3", StartPrice = 500 ,Duration = 17};
            Lot lot4 = new Lot() { Category = "TV", Name = "Lot4", Info = "Info lot4", StartPrice = 50, Duration = 1 };
          
            db.lots.AddRange(new List<Lot>() { lot1, lot2, lot3, lot4 });
            db.SaveChanges();

            Bet bet1 = new Bet() { Price = 250,Lot=lot1 };
            Bet bet2 = new Bet() { Price = 300 ,Lot=lot1};
            Bet bet3 = new Bet() { Price = 250,Lot=lot2 };
            db.bets.AddRange(new List<Bet>() { bet1, bet2, bet3 });

            db.SaveChanges();
        }
    }
    public class ExistInitializer : CreateDatabaseIfNotExists<MainContext>
    {
        protected override void Seed(MainContext db)
        {
            Lot lot1 = new Lot() { Category = "Clothes", Name = "Lot1", Info = "Info lot1", StartPrice = 200, Duration = 12 };
            Lot lot2 = new Lot() { Category = "Clothes", Name = "Lot2", Info = "Info lot2", StartPrice = 140, Duration = 6 };
            Lot lot3 = new Lot() { Category = "Toys", Name = "Lot3", Info = "Info lot3", StartPrice = 500, Duration = 17 };
            Lot lot4 = new Lot() { Category = "TV", Name = "Lot4", Info = "Info lot4", StartPrice = 50, Duration = 1 };

            db.lots.AddRange(new List<Lot>() { lot1, lot2, lot3, lot4 });
            db.SaveChanges();

            Bet bet1 = new Bet() { Price = 250, Lot = lot1 };
            Bet bet2 = new Bet() { Price = 300, Lot = lot1 };
            Bet bet3 = new Bet() { Price = 250, Lot = lot2 };
            db.bets.AddRange(new List<Bet>() { bet1, bet2, bet3 });

            db.SaveChanges();
        }
    }
}