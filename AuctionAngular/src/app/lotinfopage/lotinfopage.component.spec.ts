import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LotinfopageComponent } from './lotinfopage.component';

describe('LotinfopageComponent', () => {
  let component: LotinfopageComponent;
  let fixture: ComponentFixture<LotinfopageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LotinfopageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LotinfopageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
