import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule }   from '@angular/common/http';

import{MenubtnComponent}from './MenuButtons/menubtn.component';
import { AppComponent } from './app.component';
import {LotsComponent} from'./Lots/lots.component';
import { ContentComponent } from './mainlotspage/content.component';
import { LoginmenuComponent } from './loginmenu/loginmenu.component';

import {LotsService} from './Lots/lots.service';
import {BetsService} from './services/bet.service'
import { LayoutModule } from '@angular/cdk/layout';
import {MaterialModule} from "./material-module"
import { FormsModule } from '@angular/forms';
import { AccountstatusComponent } from './accountstatus/accountstatus.component';
import { ErrorpageComponent } from './errorpage/errorpage.component';
import { LotinfopageComponent } from './lotinfopage/lotinfopage.component';
import { AddbetdialogComponent } from './addbetdialog/addbetdialog.component';


@NgModule({
  declarations: [
    AppComponent,
    LotsComponent,
    MenubtnComponent,
    ContentComponent,
    LoginmenuComponent,
    AccountstatusComponent,
    ErrorpageComponent,
    LotinfopageComponent,
    AddbetdialogComponent
  ],
  imports: [
    MaterialModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    LayoutModule,
    FormsModule
  ],
  providers: [LotsService, BetsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
