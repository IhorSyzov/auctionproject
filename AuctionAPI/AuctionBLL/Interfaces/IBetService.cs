﻿using AuctionBLL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuctionBLL.Interfaces
{
    public interface IBetService
    {
        IEnumerable<BetDTO> GetAllBetsByLot(int LotId);
        IEnumerable<BetDTO> GetAllBetsByLot(LotDTO lot);
        Task<IEnumerable<BetDTO>> GetAllBetsByUser(string mail);
        Task AddBetOnLot(BetDTO bet, int lotId);
        void DeleteBet(BetDTO bet);
        void DeleteBet(int ID);

    }
}
