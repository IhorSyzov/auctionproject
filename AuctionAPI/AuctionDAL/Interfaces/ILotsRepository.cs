﻿using AuctionDAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuctionDAL.Interfaces
{
    public interface ILotsRepository
    {
        IEnumerable<Lot> GetAll();
        IEnumerable<Lot> GetAllByCategory(string category);
        IEnumerable<Lot> GetAllActive();
        IEnumerable<Lot> GetAllFinished();
        Task<IEnumerable<Lot>> GetUsersLotsAsync(string user);
        IEnumerable<Lot> GetLotsWhereUserPlayed(string mail);
        IEnumerable<Lot> GetAllDeleted();
        void Add(Lot lot);
        void DeleteById(int id);
        void FakeDeleteByID(int id);
        void Update(Lot lot);
        Lot GetById(int id);
        bool IsOwner(int LotId, string login);
        string GetSatus(int LotID);
        Task RejectAsync(int id);


    }
}
