﻿using AuctionBLL.Interfaces;
using AuctionBLL.Models;
using AuctionDAL.Entities;
using AuctionDAL.Interfaces;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuctionBLL.Services
{
    public class BetService: IBetService
    {
        readonly IUnitOfWork unitOfWork;
        IMapper mapper;

        public BetService(IUnitOfWork uow, IMapper mp)
        {
            unitOfWork = uow;
            mapper = mp;
        }


        /// <summary>Bets on lot.</summary>
        /// <param name="bet">The bet.</param>
        /// <param name="lotId">The lot identifier.</param>
        /// <exception cref="Exception">Bet does not exist
        /// or
        /// Lot does not exist
        /// or
        /// Not enough money
        /// or
        /// Not enough money
        /// or
        /// Bet should be bigger then other
        /// or
        /// Lot is not active</exception>
        public async Task AddBetOnLot(BetDTO bet, int lotId)
        {
            Lot lot = unitOfWork.lotsRepository.GetById(lotId);
            User user = await unitOfWork.UserManager.FindByEmailAsync(Thread.CurrentPrincipal.Identity.Name);
            IEnumerable<Bet> betlist = unitOfWork.betsRepository.GetByLotId(lotId);
            Bet betbefore = betlist.Where(p => p.Owner == user).OrderByDescending(t => t.Price).FirstOrDefault();


            if (bet is null) throw new Exception("Bet does not exist");
            if (lot is null) throw new Exception("Lot does not exist");
            if (betbefore is null && user.Wallet < bet.Price) throw new Exception("Not enough money");
            if (betbefore != null && user.Wallet < bet.Price - betbefore.Price) throw new Exception("Not enough money");
            if (betlist != null && betlist.Count() > 0)
            {
                if (betlist.OrderByDescending(p => p.Price).First().Price > bet.Price) throw new Exception("Bet should be bigger then other");
            }
            if (lot.Status != LotStatus.Active) throw new Exception("Lot is not active");
            //Withdraw
            if (betbefore is null) user.Wallet -= bet.Price;
            else user.Wallet -= bet.Price - betbefore.Price;
            ///
            Bet DBbet = new Bet() { Owner = user, OwnerLogin = user.Email, Price = bet.Price, Lot = lot };
            await unitOfWork.betsRepository.AddAsync(DBbet);

            if (lot.Bets is null) lot.Bets = new List<Bet>() { DBbet };
            else lot.Bets.Add(DBbet);
            await unitOfWork.SaveAsync();


        }

        /// <summary>Deletes the bet.</summary>
        /// <param name="bet">The bet.</param>
        /// <exception cref="Exception">Bet does not exist</exception>
        public void DeleteBet(BetDTO bet)
        {
            var mpbet = mapper.Map<BetDTO, Bet>(bet);

            if (!unitOfWork.betsRepository.Contains(mpbet)) throw new Exception("Bet does not exist");

            unitOfWork.betsRepository.Delete(mpbet);

        }

        /// <summary>Deletes the bet.</summary>
        /// <param name="Id">The identifier.</param>
        /// <exception cref="Exception">Bet does not exist</exception>
        public void DeleteBet(int Id)
        {
            var mpbet = unitOfWork.betsRepository.GetById(Id);

            if (mpbet is null) throw new Exception("Bet does not exist");

            unitOfWork.betsRepository.Delete(mpbet);
        }

        /// <summary>Gets all bets by lot.</summary>
        /// <param name="LotId">The lot identifier.</param>
        /// <returns>BetDTO list</returns>
        public IEnumerable<BetDTO> GetAllBetsByLot(int LotId)
        {
            var ls=unitOfWork.betsRepository.GetByLotId(LotId);

            return mapper.Map< IEnumerable<Bet>, IEnumerable<BetDTO>>(ls);
        }

        /// <summary>Gets all bets by lot.</summary>
        /// <param name="lot">The lot.</param>
        /// <returns>BetDTO List</returns>
        /// <exception cref="Exception">BLL.There are not bets by this lot</exception>
        public IEnumerable<BetDTO> GetAllBetsByLot(LotDTO lot)
        {
            var maplot = mapper.Map<LotDTO, Lot>(lot);
            var ls = unitOfWork.betsRepository.GetByLot(maplot);

            if (ls is null) throw new Exception("BLL.There are not bets by this lot");

            return mapper.Map<IEnumerable<Bet>, IEnumerable<BetDTO>>(ls);
        }


        /// <summary>Gets all users bets</summary>
        /// <param name="mail">The mail.</param>
        /// <returns>BetDTO list</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="Exception">User does not exist</exception>
        public async Task<IEnumerable<BetDTO>> GetAllBetsByUser(string mail)
        {
            IEnumerable<BetDTO> mappedlist = new List<BetDTO>();
            if (mail is null) throw new ArgumentNullException();
            User user=await unitOfWork.UserManager.FindByEmailAsync(mail);

            if (user is null) throw new Exception("User does not exist");

            var list=unitOfWork.betsRepository.GetByUser(mail);
            if(list !=null)mappedlist= mapper.Map<IEnumerable<Bet>, IEnumerable<BetDTO>>(list);

            return mappedlist;
        }
    }
}
