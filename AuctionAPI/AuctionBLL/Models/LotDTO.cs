﻿using System;
using System.Collections.Generic;

namespace AuctionBLL.Models
{
    public class LotDTO
    {
        public int? Id { set; get; }
        public string Name { set; get; }
        public string Info { set; get; }
        public string Category { set; get; }
        public string Status { set; get; } = "NotApproved";
        public string OwnerLogin { set; get; }
        public int Duration { set; get; }
        public int StartPrice { set; get; }
        public DateTime? CreationTime { set; get; }
        public string WinnerMail { set; get; }

        //Nav
        public ICollection<BetDTO> Bets { set; get; }
    }
}
