import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import {MatDialogRef} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import {BetsService} from '../services/bet.service'

export interface BetDialogData{
  lastbetprice:number;
  lotid:number;
}
@Component({
  selector: 'app-addbetdialog',
  templateUrl: './addbetdialog.component.html',
  styleUrls: ['./addbetdialog.component.css']
})
export class AddbetdialogComponent implements OnInit {

 inputprice!:number;
 validatetrigger:boolean=false;
 minprice!:number;

 validateMessage!:string;

  constructor(
    public dialogRef:MatDialogRef<AddbetdialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data:BetDialogData,
    private snackBar:MatSnackBar,
    private BetsService:BetsService)
     {
      this.minprice=data.lastbetprice;
    }

  ngOnInit(): void {
  }

  OnCancelClick():void{
    this.dialogRef.close();
  }

  PutBet(){
    if(sessionStorage.getItem("JWToken")==null){
      this.validatetrigger=true;
      this.validateMessage="You are not authorized";
      return;
    }
    
      this.BetsService.addBet(this.data.lotid,this.inputprice).subscribe(()=>{
        this.snackBar.open("Bet added","Hide",{duration:2000});
        this.dialogRef.close();
      },(error)=>{
        this.validateMessage=error.error.Message;
        this.validatetrigger=true; 
      });
  }

}
