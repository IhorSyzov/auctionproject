﻿namespace AuctionDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Price = c.Int(nullable: false),
                        Lot_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Lots", t => t.Lot_Id)
                .Index(t => t.Lot_Id);
            
            CreateTable(
                "dbo.Lots",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Info = c.String(),
                        Category = c.String(),
                        CreationTime = c.DateTime(),
                        Duration = c.Int(nullable: false),
                        StartPrice = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bets", "Lot_Id", "dbo.Lots");
            DropIndex("dbo.Bets", new[] { "Lot_Id" });
            DropTable("dbo.Lots");
            DropTable("dbo.Bets");
        }
    }
}
