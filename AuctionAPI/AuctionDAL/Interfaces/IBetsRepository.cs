﻿using AuctionDAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuctionDAL.Interfaces
{
    public interface IBetsRepository
    {
        IEnumerable<Bet> GetByLot(Lot lot);
        IEnumerable<Bet> GetByLotId(int id);
        IEnumerable<Bet> GetByUser(string mail);
        Bet GetById(int id);
        bool Contains(Bet bet);
        void Delete(Bet bet);
        Task AddAsync(Bet bet);
    }
}
