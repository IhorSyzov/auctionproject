﻿using AuctionAPI.DI;
using AuctionAPI.Providers;
using AuctionBLL.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Ninject;
using Owin;
using System;
using System.Web.Http.Cors;

[assembly: OwinStartup(typeof(AuctionAPI.App_Start.Startup))]

namespace AuctionAPI.App_Start
{
    public class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }
        public void Configuration(IAppBuilder app)
        {           
            app.CreatePerOwinContext<IUserService>(CreateUserService);


            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // UAuth settings
            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new AuctionOAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                //In working mode set: AllowInsecureHttp = false
                AllowInsecureHttp = true,
            };

            // Turn on beaerer tokens
            app.UseOAuthBearerTokens(OAuthOptions);
        }
        private IUserService CreateUserService()
        {
            return DIKernelFactory.Kernel.Get<IUserService>();
        }
    }
}