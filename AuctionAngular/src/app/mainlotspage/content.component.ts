import { Component, OnInit } from '@angular/core';
import { EventEmitter,Output} from '@angular/core';

@Component({
  selector: 'app-mainlotpage',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  showFiller = false;
  @Output() onChanged = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit(): void {
  }
  throwsignal(increased:any){
    this.onChanged.emit(increased);
  }

  

}
