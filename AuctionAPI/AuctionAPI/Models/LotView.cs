﻿using System;
using System.Collections.Generic;

namespace AuctionAPI.Models
{
    public class LotView
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Info { set; get; }
        public string Category { set; get; }
        public string Status { set; get; }
        public DateTime CreationTime { set; get; }
        public int Duration { set; get; }
        public int StartPrice { set; get; }
        public string WinnerMail { set; get; }

        public ICollection<BetView> Bets { set; get; }
    }
}