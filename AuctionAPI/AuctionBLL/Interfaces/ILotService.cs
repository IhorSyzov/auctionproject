﻿using AuctionBLL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuctionBLL.Interfaces
{
    public interface ILotService
    {
        IEnumerable<LotDTO> GetList();
        IEnumerable<LotDTO> GetListByCategory(string category);
        IEnumerable<LotDTO> GetListActive();
        IEnumerable<LotDTO> GetListFinished();
        Task<IEnumerable<LotDTO>> GetUsersListAsync(string login);
        IEnumerable<LotDTO> GetListWhereUserPlay(string mail);
        LotDTO GetLot(int Id);
        Task<LotDTO> AddLotAsync(LotDTO lot);
        void DeleteLot(int Id,string owner);
        void UpdateLot(LotDTO lot);
        void StartAuction(int ID);
        Task RejectAsync(int id);
        void HardDelete(int id);
        void AdminUpdateLot(LotDTO lot);
        Task<int> CheckLotEndingAsync();

    }
}
