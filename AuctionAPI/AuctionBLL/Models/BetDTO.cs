﻿namespace AuctionBLL.Models
{
    public class BetDTO
    {
        public int Id { set; get; }
        public int Price { set; get; }
        public string OwnerLogin { set; get; }

    }
}
