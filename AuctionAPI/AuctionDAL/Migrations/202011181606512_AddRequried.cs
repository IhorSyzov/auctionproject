﻿namespace AuctionDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRequried : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Bets", "Lot_Id", "dbo.Lots");
            DropIndex("dbo.Bets", new[] { "Lot_Id" });
            AlterColumn("dbo.Bets", "Lot_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Bets", "Lot_Id");
            AddForeignKey("dbo.Bets", "Lot_Id", "dbo.Lots", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bets", "Lot_Id", "dbo.Lots");
            DropIndex("dbo.Bets", new[] { "Lot_Id" });
            AlterColumn("dbo.Bets", "Lot_Id", c => c.Int());
            CreateIndex("dbo.Bets", "Lot_Id");
            AddForeignKey("dbo.Bets", "Lot_Id", "dbo.Lots", "Id");
        }
    }
}
