﻿// <auto-generated />
namespace AuctionDAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class AddIdentityToEntity : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddIdentityToEntity));
        
        string IMigrationMetadata.Id
        {
            get { return "202011151350453_AddIdentityToEntity"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
