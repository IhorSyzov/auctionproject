import {Injectable} from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
 


@Injectable()
export class BetsService{
  
    private url:string ="https://localhost:44377/api";

    constructor(private http: HttpClient){ }
      
    getBetByLotid(id:number){  
        let a =this.http.get(this.url+"/Bet/"+id.toString());
        return a;
    } 

    addBet(LotId:number,price:number){
        let body={
            "Price":price
        }

        let token=sessionStorage.getItem('JWToken');
        let myHeaders = new HttpHeaders().set('Authorization', <string>token);
          

        return this.http.put(this.url+"/bet/"+LotId.toString(),body,{headers:myHeaders});
    }
   

}