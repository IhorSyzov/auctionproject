﻿using System.ComponentModel.DataAnnotations;

namespace AuctionAPI.Models
{
    public class InputUserRole
    {
        [Required(ErrorMessage ="User id is missing")]
        public string UserId { set; get; }
        [Required(ErrorMessage = "Role is missing")]
        public string Role { set; get; }
    }
}