import { Component} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-loginmenu',
  templateUrl: './loginmenu.component.html',
  styleUrls: ['./loginmenu.component.css']
})
export class LoginmenuComponent {

  mail!:string;
  password!:string;
  validateMessage!:string;
  validatetrigger:boolean=false;

  tokenurl:string="https://localhost:44377/token";


  constructor(private http: HttpClient,public dialogRef: MatDialogRef< LoginmenuComponent>){}
    

  onNoClick(): void {
    this.dialogRef.close();
  }

  login(){
    if(this.mail==null)return;
    if(this.password==null)return;
  
    const params2=new HttpParams()
    .set("grant_type","password")
    .set("username",this.mail)
    .set("password",this.password);

    let header=new HttpHeaders().set("Content-Type","application/x-www-form-urlencoded");

    this.http.post(this.tokenurl,params2,{headers:header}).subscribe(
      (data:any)=>{
        console.log(data);
        let token="Bearer "+data.access_token;
        let tokenKey="JWToken";
        sessionStorage.setItem(tokenKey,token);
        this.dialogRef.close();
      },
    error=>{
      this.validateMessage=error.error.error_description;
      this.validatetrigger=true;        
    });
  }
  
}




