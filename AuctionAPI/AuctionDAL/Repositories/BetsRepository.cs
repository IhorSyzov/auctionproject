﻿using AuctionDAL.EF;
using AuctionDAL.Entities;
using AuctionDAL.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionDAL.Repositories
{
    public class BetsRepository : IBetsRepository
    {
        readonly MainContext db;
        public BetsRepository(MainContext context)
        {
            db = context;
        }
        public Task AddAsync(Bet bet)
        {
            return Task.Run(() =>
            {                         
                db.bets.Add(bet);
               
            });
        }

        public bool Contains(Bet bet)
        {
            return db.bets.Contains(bet);
        }

        public void Delete(Bet bet)
        {
            var list = db.lots.Where(p => p.Bets.Contains(bet));
            foreach(var a in list)
            {
                a.Bets.Remove(bet);
            }
            db.bets.Remove(bet);
        }

        public Bet GetById(int id)
        {
            return db.bets.Where(p => p.Id == id).FirstOrDefault();
        }

        public IEnumerable<Bet> GetByLot(Lot lot)
        {
           Lot a= db.lots.Find(lot);
            return a.Bets.ToList();
        }

        public IEnumerable<Bet> GetByLotId(int id)
        {
            var ls = (from t in db.bets where t.Lot.Id == id select t).Include(p => p.Owner);
            return ls.ToList();
        }

        public IEnumerable<Bet> GetByUser(string mail)
        {
            User user = db.Users.Where(p => p.Email == mail).Include(p => p.bets).FirstOrDefault();
            return user.bets.ToList();
        }
    }
}