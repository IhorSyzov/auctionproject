﻿using System.ComponentModel.DataAnnotations;

namespace AuctionAPI.Models
{
    public class LotInput
    {   [Required(ErrorMessage ="Name is empty")]
        [StringLength(35,MinimumLength =6,ErrorMessage ="Length should be 6-35 letters")]
        public string Name { set; get; }

        [Required(ErrorMessage ="Info is empty")]
        [StringLength(35, MinimumLength = 6, ErrorMessage = "Length should be 6-35 letters")]
        public string Info { set; get; }

        [StringLength(20, MinimumLength = 3, ErrorMessage = "Length should be 3-20 letters")]
        [Required(ErrorMessage ="Category field is empty")]
        public string Category { set; get; }

        [Required(ErrorMessage ="Duration field is empty")]
        [Range(1,168,ErrorMessage ="Max duration - 7 days")]
        public int Duration { set; get; }

        [Required]
        [Range(1,int.MaxValue, ErrorMessage = "Price can not be less than 0")]
        public int StartPrice { set; get; }       

    }
}