import {  OnInit,AfterViewInit, Component, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {Lot} from '../Lots/Lot';
import {MatDialog} from '@angular/material/dialog';
import {LotsService} from '../Lots/lots.service';
import {BetsService} from '../services/bet.service'
import {AddbetdialogComponent} from '../addbetdialog/addbetdialog.component'

export interface PeriodicElement {
  nickname: string;
  price: number;
  id:number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {nickname:"aSAdasd",price:2000,id:5},
  {nickname:"jkhkjhkhj",price:2220,id:6},
  {nickname:"aSewwwwd",price:3000,id:7}
  
];

export interface IBet{
  Id:number;
  Price:number;
  OwnerLogin:string;
}



@Component({
  selector: 'app-lotinfopage',
  templateUrl: './lotinfopage.component.html',
  styleUrls: ['./lotinfopage.component.css']
})
export class LotinfopageComponent implements OnInit {

  lotid!:number;
  CurrentLot!:Lot;

  MyBets!:IBet[];
  dataSource!:MatTableDataSource<IBet>;

  displayedColumns: string[] = ['Id', 'OwnerLogin', 'Price'];

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private activateRoute : ActivatedRoute,
    private LotsService: LotsService,
    private BetsService: BetsService,
    public dailog: MatDialog) 
    { 
    this.lotid=activateRoute.snapshot.params['id'];
    }


  ngOnInit(): void {
    this.LotsService.getLot(this.lotid).subscribe((data)=>{
    this.CurrentLot=<Lot>data});
    
    this.UpdateBets();
  }

  UpdateBets(){
    this.BetsService.getBetByLotid(this.lotid).subscribe((data)=>{
      this.MyBets=<IBet[]>data;
      this.MyBets.sort((a:IBet,b:IBet):number=>-(<number>a.Price-<number>b.Price));
      this.dataSource= new MatTableDataSource<IBet>(this.MyBets); 
      this.dataSource.paginator = this.paginator;    
    })
  }
  
  OpenBetMenu(){
    let price:number=this.CurrentLot.StartPrice;
    if(this.MyBets.length>0)price=this.MyBets[0].Price;
    const dialogRef=this.dailog.open(AddbetdialogComponent,{
      width:'300px',
      height:'250px',
      data:{lastbetprice:price,lotid:this.CurrentLot.Id }
    });
    dialogRef.afterClosed().subscribe(()=>{
      this.UpdateBets();
    });

    
  }


}
