﻿using AuctionBLL.Interfaces;
using AuctionBLL.Models;
using AuctionDAL.Entities;
using AuctionDAL.Interfaces;
using AutoMapper;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace AuctionBLL.Services
{
    public class UserService : IUserService
    {
        IUnitOfWork Database { get; }
        IMapper mapper { get; }

        public UserService(IUnitOfWork db,IMapper mp)
        {
            Database = db;
            mapper = mp;
        }


        /// <summary>Create new account</summary>
        /// <param name="userDto">The user dto.</param>
        /// <exception cref="Exception">Creating account error
        /// or
        /// This account already exists</exception>
        public async Task CreateAsync(UserDTO userDto)
        {
            User user = await Database.UserManager.FindByEmailAsync(userDto.Email);
            if (user == null)
            {
                user = new User { Email = userDto.Email, UserName = userDto.Email };
                var result = await Database.UserManager.CreateAsync(user, userDto.Password);
                if (result.Errors.Count() > 0)
                    throw new Exception("Creating account error");
                //Adding role
                foreach (var a in userDto.Roles)
                {
                    await Database.UserManager.AddToRoleAsync(user.Id, a);
                }
                Database.Save();
                return;
            }
            else
            {
                throw new Exception("This account already exists");
            }
        }


        /// <summary>Sets the initial data.</summary>
        /// <param name="adminDto">The admin dto.</param>
        /// <param name="roles">The roles.</param>
        public async Task SetInitialDataAsync(UserDTO adminDto, List<string> roles)
        {
            foreach (string roleName in roles)
            {
                var role = await Database.RoleManager.FindByNameAsync(roleName);
                if (role == null)
                {
                    role = new Role { Name = roleName };
                    await Database.RoleManager.CreateAsync(role);
                }
            }
            await CreateAsync(adminDto);
            Database.Save();
        }


        /// <summary>Finds the user asynchronous.</summary>
        /// <param name="login">The login.</param>
        /// <param name="pass">The pass.</param>
        /// <returns>User</returns>
        /// <exception cref="ArgumentNullException">login/pass null</exception>
        public async Task<UserDTO> FindUserAsync(string login,string pass)
        {
            if (login is null || pass is null) throw new ArgumentNullException("login/pass null");

            User user=await Database.UserManager.FindAsync(login, pass);
            if (user is null) return null;

            UserDTO dto = mapper.Map<User, UserDTO>(user);
            return dto;
            
        }


        /// <summary>Finds the user asynchronous.</summary>
        /// <param name="ID">The identifier.</param>
        /// <returns>User</returns>
        /// <exception cref="ArgumentNullException">login/pass null</exception>
        public async Task<UserDTO> FindUserAsync(string ID)
        {
            if (ID is null) throw new ArgumentNullException("login/pass null");

            User user = await Database.UserManager.FindByIdAsync(ID);
            if (user is null) return null;

            UserDTO dto = mapper.Map<User, UserDTO>(user);
            return dto;

        }


        /// <summary>Deletes the user asynchronous.</summary>
        /// <param name="id">The identifier.</param>
        /// <exception cref="ArgumentNullException">id is null</exception>
        /// <exception cref="Exception">Wrong id</exception>
        public async Task DeleteAsync(string id)
        {
            if (id is null) throw new ArgumentNullException("id is null");

            User user =await Database.UserManager.FindByIdAsync(id);
            if (user is null) throw new Exception("Wrong id");

            await Database.UserManager.DeleteAsync(user);
            await Database.SaveAsync();
        }


        /// <summary>Generates the identity asynchronous.</summary>
        /// <param name="login">The login.</param>
        /// <param name="pass">The pass.</param>
        /// <param name="authenticationType">Type of the authentication.</param>
        /// <returns>Identity</returns>
        /// <exception cref="ArgumentNullException">login/pass</exception>
        /// <exception cref="Exception">User does not exist</exception>
        public async Task<ClaimsIdentity> GenerateIdentityAsync(string login, string pass, string authenticationType)
        {
            if (login is null || pass is null) throw new ArgumentException("login/pass");
            User user = await Database.UserManager.FindAsync(login, pass);
            if (user is null) throw new ArgumentException("User does not exist");

            var userIdentity = await Database.UserManager.CreateIdentityAsync(user, authenticationType);

            return userIdentity;
        }


        /// <summary>Gets the users list</summary>
        /// <returns>users list</returns>
        public IEnumerable<UserDTO> GetUsers()
        {
            var list = Database.UserManager.Users.Include(p => p.Roles).ToList();

           var mapped= mapper.Map<IEnumerable<User>, IEnumerable<UserDTO>>(list);
            return mapped;

        }


        /// <summary>Gets the user by mail.</summary>
        /// <param name="mail">The mail.</param>
        /// <returns>User</returns>
        /// <exception cref="ArgumentNullException">Email is null</exception>
        /// <exception cref="Exception">User does not exist</exception>
        public async Task<UserDTO> GetUserByMailAsync(string mail)
        {
            if (mail is null) throw new ArgumentNullException("Email is null");

           User userDB= await Database.UserManager.FindByEmailAsync(mail);

            if (userDB is null) throw new Exception("User does not exist");

            UserDTO user = mapper.Map<User, UserDTO>(userDB);
            return user;
        }


        /// <summary>Changes pass of current user</summary>
        /// <param name="oldpass">The oldpass.</param>
        /// <param name="newpass">The newpass.</param>
        /// <exception cref="ArgumentNullException">pass</exception>
        /// <exception cref="Exception">User does not exist</exception>
        public async Task ChangeMyPassAsync(string oldpass,string newpass)
        {
            string login = Thread.CurrentPrincipal.Identity.Name;
            User user = await Database.UserManager.FindByEmailAsync(login);

            if (oldpass is null || newpass is null) throw new ArgumentNullException("pass");
            if (user is null) throw new Exception("User does not exist");

           var a= await Database.UserManager.ChangePasswordAsync(user.Id, oldpass, newpass);

            await Database.SaveAsync();
        }


        /// <summary>Updates the user bio.</summary>
        /// <param name="user">The user.</param>
        public async Task UpdateUserBioAsync(UserDTO user)
        {
            string mail = Thread.CurrentPrincipal.Identity.Name;
            User dbuser = await Database.UserManager.FindByEmailAsync(mail);
            //Phone number
            if (user.PhoneNumber != null)
            {
                dbuser.PhoneNumber = user.PhoneNumber;
            }
            //Name
            await Database.SaveAsync();
        }


        /// <summary>Deletes the account of curent user</summary>
        public async Task DeleteMyAccountAsync()
        {
            string mail = Thread.CurrentPrincipal.Identity.Name;
            User user = Database.UserManager.FindByEmail(mail);

            await Database.UserManager.DeleteAsync(user);
            await Database.SaveAsync();
           
        }


        /// <summary>Adds the role to user</summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="role">The role.</param>
        /// <exception cref="ArgumentNullException">userid or role</exception>
        /// <exception cref="Exception">User does not exist
        /// or
        /// Role does not exist</exception>
        public async Task AddRoleAsync(string userId,string role)
        {
            User user;
            Role rol;
            if (userId is null || role is null) throw new ArgumentNullException("userid or role");

            user = await Database.UserManager.FindByIdAsync(userId);
            rol = await Database.RoleManager.FindByNameAsync(role);

            if (user is null) throw new Exception("User does not exist");
            if (rol is null) throw new Exception("Role does not exist");

            await Database.UserManager.AddToRoleAsync(userId, role);

        }


        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
