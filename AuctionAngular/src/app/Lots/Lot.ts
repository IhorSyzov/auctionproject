export class Lot{
    
        Id!: number;
        Name!: string;
        Info!:string;
        Category!:string;
        Status!:string;
        CreationTime!: Date;
        Duration!:number;
        StartPrice!:number;
        WinnerMail!:string;

        Bets!: Bet[];
        
      
}
export interface Bet{
  Id:number;
  Price:number;
  OwnerLogin:string;
}