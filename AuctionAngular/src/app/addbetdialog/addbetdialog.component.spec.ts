import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddbetdialogComponent } from './addbetdialog.component';

describe('AddbetdialogComponent', () => {
  let component: AddbetdialogComponent;
  let fixture: ComponentFixture<AddbetdialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddbetdialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddbetdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
