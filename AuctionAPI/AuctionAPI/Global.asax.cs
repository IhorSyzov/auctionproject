using AuctionAPI.DI;
using AuctionBLL.Interfaces;
using Ninject;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace AuctionAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private static Timer timer;
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            //DI
            var ninjectResolver = new NinjectDependencyResolver(DIKernelFactory.Kernel);
            GlobalConfiguration.Configuration.DependencyResolver = ninjectResolver;
            //Lots Updater            
            StandardKernel kernel = DIKernelFactory.Kernel;
            TimerCallback tm = new TimerCallback(LotUpdate);       
            timer = new Timer(tm, kernel, 1000 * 10, Timeout.Infinite);
            //
        }

        private void LotUpdate(object DIkernel)
        {
            timer.Dispose();

            StandardKernel kern = (StandardKernel)DIkernel;
            ILotService service=kern.Get<ILotService>();

             service.CheckLotEndingAsync();

            timer = new Timer(LotUpdate, DIkernel, 1000 * 60, Timeout.Infinite);
        }
    }
}
