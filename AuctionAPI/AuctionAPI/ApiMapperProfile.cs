﻿using AuctionAPI.Models;
using AuctionBLL.Models;
using AutoMapper;

namespace AuctionAPI
{
	public class ApiMapperProfile:Profile
	{
		public ApiMapperProfile()
		{
			CreateMap<BetView, BetDTO>().ReverseMap();
			CreateMap<LotView, LotDTO>().ReverseMap();
			CreateMap<BetInput, BetDTO>();
			CreateMap<RegistrationModel, UserDTO>();
			CreateMap<UpdateBioModel, UserDTO>().ForMember(p => p.PhoneNumber, t => t.MapFrom(v => v.Number));
			CreateMap<LotInput, LotDTO>();
		}
	}
}