import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
 


@Injectable()
export class LotsService{
  
    private url:string ="https://localhost:44377/api";

    constructor(private http: HttpClient){ }
      
    getLots(){  
        //let a=this.http.get('../../assets/test.json');
        let a =this.http.get(this.url+"/Lot");
        return a;
    } 
    
    getLot(id:number){
        let a= this.http.get(this.url+"/Lot/"+id.toString());
        return a;
    }
   

}