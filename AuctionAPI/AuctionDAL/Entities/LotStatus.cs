﻿namespace AuctionDAL.Entities
{
    public enum LotStatus
    {
        NotApproved,
        Active,
        Finished,
        Rejected
    }
}