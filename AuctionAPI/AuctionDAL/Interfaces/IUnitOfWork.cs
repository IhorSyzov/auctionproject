﻿using AuctionDAL.Identity;
using System;
using System.Threading.Tasks;

namespace AuctionDAL.Interfaces
{
    public interface IUnitOfWork:IDisposable
    {
        IBetsRepository betsRepository { get; }
        ILotsRepository lotsRepository { get; }
        AppUserManager UserManager { get; }
        AppRoleManager RoleManager { get; }

        Task<int> SaveAsync();
        void Save();
    }
}
