﻿using AuctionDAL.EF;
using AuctionDAL.Entities;
using AuctionDAL.Identity;
using AuctionDAL.Interfaces;
using AuctionDAL.Repositories;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;

namespace AuctionDAL
{
    public class UnitOfWork : IUnitOfWork
    {
        public IBetsRepository betsRepository { get; }

        public ILotsRepository lotsRepository { get; }

        public AppUserManager UserManager { get; }

        public AppRoleManager RoleManager { get; }


        readonly MainContext db;
        public UnitOfWork(MainContext context)
        {           
             betsRepository = new BetsRepository(context);
            lotsRepository = new LotsRepository(context);
            UserManager = new AppUserManager(new UserStore<User>(context));
            RoleManager = new AppRoleManager(new RoleStore<Role>(context));
            db = context;


        }

        public async Task<int> SaveAsync()
        {
            return await db.SaveChangesAsync();
        }
        public void Save()
        {
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
            UserManager.Dispose();
            RoleManager.Dispose();
        }
        public void Dispose(bool IsFull)
        {

        }
    }
}