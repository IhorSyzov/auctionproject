import { Component, OnInit} from '@angular/core';
import {Lot} from './Lot';
import { LotsService} from './lots.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-lotlist',
  templateUrl: './lots.component.html',
  styleUrls: ['./lots.component.css']
})
export class LotsComponent {

  lots!: Lot[];

  constructor(private LotsService: LotsService, private router: Router){}
    
  ngOnInit(){
       this.GetLots();     
  }
  GetLots(){
    this.LotsService.getLots().subscribe((data)=>{
      this.lots=<Lot[]>data});
  }

 openLot(id:number){
   this.router.navigate(['lot',id]);
 }
      
}