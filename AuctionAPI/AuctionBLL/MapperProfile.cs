﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuctionBLL.Models;
using AuctionDAL.Entities;

namespace AuctionBLL
{
    public class MapperProfile :Profile
    {
        public MapperProfile()
        {
            CreateMap<BetDTO, Bet>().ReverseMap();
            CreateMap<LotDTO, Lot>().ReverseMap();
            CreateMap<User, UserDTO>().ForMember(p=>p.Roles,a=>a.Ignore()).ReverseMap();
            CreateMap<LotStatus, string>().ConvertUsing(src => src.ToString());
            CreateMap<string, LotStatus>().ConvertUsing(src => MapStatus(src));
            
        }
        public static LotStatus MapStatus(string grade)
        {
            LotStatus stat;
            if (!Enum.TryParse<LotStatus>(grade, out stat)) throw new AutoMapperMappingException("Enum parse error");
            
            return stat;
        }
    }
}
