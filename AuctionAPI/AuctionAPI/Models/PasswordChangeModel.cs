﻿using System.ComponentModel.DataAnnotations;

namespace AuctionAPI.Models
{
    public class PasswordChangeModel
    {
        [Required(ErrorMessage ="No old password")]
        public string Old { set; get; }

        [Required(ErrorMessage = "No new password")]
        [StringLength(35, MinimumLength = 6, ErrorMessage = "Password should be 6-20 letters")]
        public string New { set; get; }
    }
}