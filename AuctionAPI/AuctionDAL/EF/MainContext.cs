﻿using AuctionDAL.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace AuctionDAL.EF
{
    public class MainContext:IdentityDbContext<User>
    {
        static MainContext()
        {
            Database.SetInitializer<MainContext>(new ExistInitializer());

        }
        public MainContext() : base("AuctionProject")
        {

        }
        public DbSet<Lot> lots { set; get; }
        public DbSet<Bet> bets { set; get; }
    }
}