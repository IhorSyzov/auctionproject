﻿using AuctionDAL.EF;
using AuctionDAL.Entities;
using AuctionDAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace AuctionDAL.Repositories
{
    public class LotsRepository : ILotsRepository
    {
        readonly MainContext db;
        public LotsRepository(MainContext context)
        {
            db = context;
        }
        public void Add(Lot lot)
        {
            db.lots.Add(lot);
        }

        public bool IsOwner(int LotId,string login)
        {
            var a=db.lots.Where(p => p.Id == LotId).FirstOrDefault();
            if (a.OwnerLogin == login) return true;
            else return false;
        }

        public string GetSatus(int LotID)
        {
            var a = db.lots.Where(p => p.Id == LotID).FirstOrDefault();
            return a.Status.ToString();
        }        

        public void DeleteById(int id)
        {
            var lot=(from t in db.lots where t.Id==id select t).FirstOrDefault();
            if (lot != null) db.lots.Remove(lot);
        }

        public void FakeDeleteByID(int id)
        {
            var a = db.lots.Where(b => b.Id == id).FirstOrDefault();
            if(a !=null)a.Deleted = true;
        }

        public IEnumerable<Lot> GetAll()
        {
           var a=db.lots.Include(p=>p.Bets).Where(p => p.Deleted == false).ToList();
            return a;
        }

        public IEnumerable<Lot> GetAllActive()
        {
            return db.lots.Where(p => p.Deleted == false && p.Status == LotStatus.Active).
                Include(p => p.Bets).ToList();
        }

        public IEnumerable<Lot> GetAllByCategory(string category)
        {
            return db.lots.Where(p => p.Deleted == false && p.Category==category).ToList();
        }

        public IEnumerable<Lot> GetAllFinished()
        {
            return db.lots.Where(p => p.Deleted == false && p.Status==LotStatus.Finished).ToList();
        }

        public Lot GetById(int id)
        {
            return db.lots.Where(p => p.Id == id).FirstOrDefault();
        }


        public IEnumerable<Lot> GetLotsWhereUserPlayed(string mail)
        {
            IEnumerable<Lot> ls = db.lots.Where(p => p.Bets.Where(t => t.OwnerLogin == mail).Count() > 0)
                .Include(g=>g.Bets).ToList();
            return ls;
        }

        public async Task<IEnumerable<Lot>> GetUsersLotsAsync(string user)
        {
           return await Task.Run(()=> db.lots.Where(p => p.OwnerLogin == user));
        }

        public void Update(Lot lot)
        {
            //not update bets
            var dblot = db.lots.Where(p => p.Id == lot.Id).FirstOrDefault();

            if (dblot is null) throw new ArgumentNullException();

            dblot.Category = lot.Category;
            dblot.Duration = lot.Duration;
            dblot.Info = lot.Info;
            dblot.Name = lot.Name;
            dblot.StartPrice = lot.StartPrice;           
        }

        public IEnumerable<Lot> GetAllDeleted()
        {
            return db.lots.Where(p => p.Deleted == true).ToList();
        }

        public async Task RejectAsync(int id)
        {
            await Task.Run(() =>
            {
                Lot lot = db.lots.Where(p => p.Id == id).FirstOrDefault();
                lot.Status = LotStatus.Rejected;


            });
        }


    }
}