﻿using AuctionBLL.DI;
using Ninject;
using Ninject.Modules;

namespace AuctionAPI.DI
{
    public static class DIKernelFactory
    {
        static StandardKernel kernel;
        public static StandardKernel Kernel
        {
            get
            {
                if (kernel is null)
                {
                    NinjectModule registrations = new ApiDIModule();
                    NinjectModule bll = new BLLNinjectModule();
                    kernel = new StandardKernel(registrations, bll);
                }
                return kernel;
            }
        }
    }
}