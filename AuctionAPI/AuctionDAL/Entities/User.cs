﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace AuctionDAL.Entities
{
    public class User : IdentityUser
    {
        public ICollection<Lot> lots { set; get; }
        public ICollection<Bet> bets{set;get;}
        public int Wallet { set; get; }
    }
}