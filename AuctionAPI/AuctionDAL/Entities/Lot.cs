﻿using System;
using System.Collections.Generic;

namespace AuctionDAL.Entities
{
    public class Lot
    {
        public int Id { set; get; }
        public int Duration { set; get; }
        public int StartPrice { set; get; }
        public string Name { set; get; }
        public string Info { set; get; }
        public string Category { set; get; }
        public string OwnerLogin { set; get; }
        public bool Deleted { set; get; } = false;
        public DateTime? CreationTime { set; get; }
        public LotStatus Status { set; get; } = LotStatus.NotApproved;
        public string WinnerMail { set; get; }


        //Navigation
        public ICollection<Bet> Bets { set; get; }
        public User Owner { set; get; }
    }
}