﻿using System.Collections.Generic;

namespace AuctionBLL.Models
{
    public class UserDTO
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { set; get; }
        public int Wallet { set; get; }

        public ICollection<string> Roles { get; set; }

    }
}
