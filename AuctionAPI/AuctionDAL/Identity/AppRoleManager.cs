﻿using AuctionDAL.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AuctionDAL.Identity
{
    public class AppRoleManager:RoleManager<Role>
    {
        public AppRoleManager(RoleStore<Role> store)
                    : base(store)
        { }
    }
}