﻿namespace AuctionAPI.Models
{
    public class BetView
    {
        public int Id { set; get; }
        public int Price { set; get; }
        public string OwnerLogin { set; get; }

    }
}