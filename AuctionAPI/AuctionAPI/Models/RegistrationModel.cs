﻿using System.ComponentModel.DataAnnotations;

namespace AuctionAPI.Models
{
    public class RegistrationModel
    {   [Required(ErrorMessage ="Email is empty")]
        [EmailAddress(ErrorMessage = "Wrong email format")]
        public string Email { set; get; }

        [Required(ErrorMessage = "No new password")]
        [StringLength(35, MinimumLength = 6, ErrorMessage = "Password should be 6-20 letters")]
        public string Password { set; get; }
    }
}