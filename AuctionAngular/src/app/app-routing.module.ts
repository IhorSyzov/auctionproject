import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{ContentComponent} from './mainlotspage/content.component';
import{ErrorpageComponent} from './errorpage/errorpage.component';
import {LotinfopageComponent} from './lotinfopage/lotinfopage.component';


const routes: Routes = [
{path:'',component:ContentComponent},
{path:"lot/:id",component:LotinfopageComponent},
{path:'**',component:ErrorpageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
