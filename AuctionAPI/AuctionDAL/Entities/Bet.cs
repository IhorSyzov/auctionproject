﻿using System.ComponentModel.DataAnnotations;

namespace AuctionDAL.Entities
{
    public class Bet
    {
        public int Id { set; get; }
        public int Price { set; get; }       
        public string OwnerLogin { set; get; }


        //Navigation
        [Required]
        public Lot Lot { set; get; }
        public User Owner { set; get; }

    }
}