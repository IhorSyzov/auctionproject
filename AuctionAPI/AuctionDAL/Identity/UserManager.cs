﻿using AuctionDAL.Entities;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuctionDAL.Identity
{
    public class AppUserManager : UserManager<User>
    {
        public AppUserManager(IUserStore<User> store)
                : base(store)
        {
        }

        public void AddLot(User user, Lot lot)
        {
            if (user.lots is null) user.lots = new List<Lot>();
            user.lots.Add(lot);
            lot.Owner = user;
        }

        public async Task AddMoney(string userId,int money)
        {
           User user= await Store.FindByIdAsync(userId);
            user.Wallet += money;
           
        }
      
    }
}