﻿using AuctionAPI.Models;
using AuctionBLL.Interfaces;
using AuctionBLL.Models;
using AutoMapper;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace AuctionAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/bet")]
    [ExceptionsCheck]
    public class BetController : ApiController
    {
        readonly IMapper mapper;
        readonly IBetService service;

        public BetController(IBetService serv, IMapper map)
        {
            mapper = map;
            service = serv;
        }



        /// <summary>Gets bets of current user</summary>
        /// <returns>IEnumerableBetDTO</returns>
        [Route("My")]
        public async Task<IEnumerable<BetDTO>> GetMyBets()
        {
            string mail = Thread.CurrentPrincipal.Identity.Name;
           return await service.GetAllBetsByUser(mail);
        }


        /// <summary>Adds the new bet.</summary>
        /// <param name="bet">The bet.</param>
        /// <param name="lotId">The lot identifier.</param>
        /// <returns>200/400 Status Code</returns>
        [Route("{lotId:int}")]
        [HttpPut]
        public async Task<IHttpActionResult> AddNewBet(BetInput bet, int lotId)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            BetDTO dto = mapper.Map<BetInput, BetDTO>(bet);

            await service.AddBetOnLot(dto, lotId);

            return Ok();
        }


        /// <summary>Gets bets by lot.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>List of Bets</returns>
        [AllowAnonymous]
        [Route("{id:int}")]
        public IEnumerable<BetDTO> GetBetsByLot(int id)
        {
            return service.GetAllBetsByLot(id);
        }


        /// <summary>Admin function. Get Bets by user email.</summary>
        /// <param name="mail">The mail.</param>
        /// <returns>Bets list</returns>
        [Authorize(Roles =("admin"))]
        [Route("admin/user/{mail}")]
        public async Task<IEnumerable<BetDTO>> GetUsersBets(string mail)
        {
            return await service.GetAllBetsByUser(mail);
        }



    }
}
