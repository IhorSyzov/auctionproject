﻿using System.ComponentModel.DataAnnotations;

namespace AuctionAPI.Models
{
    public class BetInput
    {
        [Required(ErrorMessage ="Price is empty")]
        [Range(0,int.MaxValue,ErrorMessage ="Value less than 0")]
        public int Price { set; get; }
    }
}