﻿namespace AuctionDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LotWinner : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Lots", "WinnerMail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Lots", "WinnerMail");
        }
    }
}
