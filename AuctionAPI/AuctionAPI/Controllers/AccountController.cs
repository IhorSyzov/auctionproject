﻿using AuctionAPI.Models;
using AuctionAPI.Providers;
using AuctionBLL.Interfaces;
using AuctionBLL.Models;
using AutoMapper;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;


namespace AuctionAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/account")]
    [ExceptionsCheck]
    public class AccountController : ApiController
    {
        readonly IMapper mapper;
        readonly IUserService service;

        public AccountController(IUserService serv, IMapper map)
        {
            mapper = map;
            service = serv;
        }


        /// <summary>Register the user</summary>
        /// <param name="model">Registration Model</param>
        /// <returns>Status id 200 /400</returns>
        [AllowAnonymous]
        [Route("register")]
        public async Task<IHttpActionResult> PostRegister(RegistrationModel model)
        {
            string baseRole = "user";
            UserDTO user;

            if (!ModelState.IsValid) return BadRequest(ModelState);

            user = mapper.Map<RegistrationModel, UserDTO>(model);
            user.Roles = new List<string>() { baseRole };

            await service.CreateAsync(user);


            return Ok();
        }


        /// <summary>Sets the initialize data.
        /// Adds roles and admin account</summary>
        /// <returns>200/400 HTTP Status Code</returns>
        [AllowAnonymous]
        [Route("init")]
        [HttpGet]
        public IHttpActionResult SetInitData()
        {
            List<string> RoleList = new List<string> { "user", "admin" };
            var email = ConfigurationManager.AppSettings["AdminEmail"];
            var pass = ConfigurationManager.AppSettings["AdminPassword"];

            UserDTO user = new UserDTO() { Email = email, Password = pass, Roles = new List<string> { "admin" } };

            service.SetInitialDataAsync(user, RoleList);
            return Ok();
        }


        //[Route("api/account")]
        /// <summary>Gets the information about current user account.</summary>       
        /// <returns>UserDTO</returns>
        public async Task<UserDTO> GetInfoAboutMyAccount()
        {
            string mail = Thread.CurrentPrincipal.Identity.Name;
            UserDTO user=await service.GetUserByMailAsync(mail);
            return user;
        }


        /// <summary>Change password of current user</summary>
        /// <param name="model" type="PasswordChangeModel">New and old passwords</param>
        /// <returns>200/400 HTTP Status Code</returns>
        [Route("pass")]
        public async Task<IHttpActionResult> PostChangePass(PasswordChangeModel model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            await service.ChangeMyPassAsync(model.Old, model.New);

            return Ok();
        }


        // POST api/account       
        /// <summary>Update info of current user</summary>
        /// <param name="model">The model.</param>
        /// <returns>200/400 HTTP Status Code</returns>
        public async Task<IHttpActionResult> PostUpdateAccountInfo(UpdateBioModel model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            UserDTO mapped = mapper.Map<UpdateBioModel, UserDTO>(model);

            await service.UpdateUserBioAsync(mapped);
            return Ok();
        }


        // DELETE api/account
        /// <summary>Deletes account of current user</summary>
        /// <returns>200/400 HTTP Status Code</returns>
        public async Task<IHttpActionResult> DeleteMyAccount()
        {
            await service.DeleteMyAccountAsync();
            Request.GetOwinContext().Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }


        /// <summary>Logout from account</summary>
        /// <returns>200/400 HTTP Status Code</returns>
        [Route("logout")]
        public IHttpActionResult GetLogout()
        {
            Request.GetOwinContext().Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }



        /// <summary>Gets the user by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>UserDTO</returns>
        [Authorize(Roles = "admin")]
        [Route("admin/user/{id}")]
        public async Task<UserDTO> GetUserByID(string id)
        {
            UserDTO user = await service.FindUserAsync(id);
            return user;
        }


        /// <summary>Admin function. Delete user.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>200/400 Http Status Code</returns>
        [Authorize(Roles = "admin")]
        [Route("admin/user/{id}")]
        public async Task<IHttpActionResult> Delete(string id)
        {
            await service.DeleteAsync(id);
            return Ok();
        }


        /// <summary>Admin function. Gets the users.</summary>
        /// <returns>IEnumerableUserDTO</returns>
        [Authorize(Roles = "admin")]
        [Route("admin/user")]
        public IEnumerable<UserDTO> GetUsers()
        {
            var list = service.GetUsers();

            return list;
        }


        /// <summary>Admin function. Add role to the user</summary>
        /// <param name="model">Id and role</param>
        /// <returns>200/400 HTTP Status Code</returns>
        [Authorize(Roles = "admin")]
        [Route("admin/user/role")]
        public async Task<IHttpActionResult> PutUserRole(InputUserRole model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            await service.AddRoleAsync(model.UserId, model.Role);
            return Ok();
        }


    }
}
