import { Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { LoginmenuComponent } from '../loginmenu/loginmenu.component';



@Component({
  selector: 'menubtns',
  templateUrl: './menubtn.component.html',
  styleUrls: ['./menubtn.component.css']
})
export class MenubtnComponent {
  
  buttonNames:string[]=["Lots","Finished Lots"];
 dictionary:{[name:string]:string}={
     "Lots":"",
     "Finsihed Lots":"url"
 }
 animal!: string;
 name!: string;

 constructor(public dialog: MatDialog){

 }

 openDialog(): void {
  const dialogRef = this.dialog.open(LoginmenuComponent, {
    width: '250px',
    height:'300px'
  });

  
}
}