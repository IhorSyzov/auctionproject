﻿namespace AuctionDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIdentityToEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bets", "OwnerLogin", c => c.String());
            AddColumn("dbo.Bets", "Owner_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.Lots", "OwnerLogin", c => c.String());
            AddColumn("dbo.Lots", "Owner_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Bets", "Owner_Id");
            CreateIndex("dbo.Lots", "Owner_Id");
            AddForeignKey("dbo.Lots", "Owner_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Bets", "Owner_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bets", "Owner_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Lots", "Owner_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Lots", new[] { "Owner_Id" });
            DropIndex("dbo.Bets", new[] { "Owner_Id" });
            DropColumn("dbo.Lots", "Owner_Id");
            DropColumn("dbo.Lots", "OwnerLogin");
            DropColumn("dbo.Bets", "Owner_Id");
            DropColumn("dbo.Bets", "OwnerLogin");
        }
    }
}
