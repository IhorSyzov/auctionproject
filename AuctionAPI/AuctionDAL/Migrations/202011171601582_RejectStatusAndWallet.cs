﻿namespace AuctionDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RejectStatusAndWallet : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Wallet", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Wallet");
        }
    }
}
