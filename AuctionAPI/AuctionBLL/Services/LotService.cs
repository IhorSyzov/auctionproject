﻿using AuctionBLL.Interfaces;
using AuctionBLL.Models;
using AuctionDAL.Entities;
using AuctionDAL.Interfaces;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace AuctionBLL.Services
{
    public class LotService : ILotService
    {
        readonly IUnitOfWork unitOfWork;
        IMapper mapper;

        public LotService(IUnitOfWork uow,IMapper mp)
        {
            unitOfWork = uow;
            mapper = mp;
        }

        /// <summary>Adds the lot in db asynchronous.</summary>
        /// <param name="lot">The lot.</param>
        /// <returns>LotDTO</returns>
        /// <exception cref="Exception">User does not exist</exception>
        public async Task<LotDTO> AddLotAsync(LotDTO lot)
        {
            Lot mappedlot = mapper.Map<LotDTO, Lot>(lot);
            User user= await unitOfWork.UserManager.FindByEmailAsync(mappedlot.OwnerLogin);

            if (user is null) throw new Exception("User does not exist");

            unitOfWork.lotsRepository.Add(mappedlot);
            unitOfWork.Save();

            unitOfWork.UserManager.AddLot(user, mappedlot);
            unitOfWork.Save();

            var answ = mapper.Map<Lot, LotDTO>(mappedlot);
            return answ;
        }


        /// <summary>Deletes the lot.</summary>
        /// <param name="Id">The identifier.</param>
        /// <param name="owner">The owner.</param>
        /// <exception cref="Exception">Lot with id {Id} does not exist</exception>
        /// <exception cref="FieldAccessException">No permissions to delete this lot</exception>
        public void DeleteLot(int Id,string owner)
        {
            var lot=unitOfWork.lotsRepository.GetById(Id);

            if (lot is null) throw new Exception($"Lot with id {Id} does not exist");
            if (lot.OwnerLogin != owner) throw new FieldAccessException("No permissions to delete this lot");

            unitOfWork.lotsRepository.FakeDeleteByID(Id);
            unitOfWork.Save();
        }


        /// <summary>Hard delete lot.</summary>
        /// <param name="id">The identifier.</param>
        /// <exception cref="Exception">Lot does not exist</exception>
        public void HardDelete(int id)
        {
            Lot lot=unitOfWork.lotsRepository.GetById(id);

            if (lot is null) throw new Exception("Lot does not exist");

            unitOfWork.lotsRepository.DeleteById(id);
            unitOfWork.Save();
        }


        /// <summary>Gets the list.</summary>
        /// <returns>List of LotDTOs</returns>
        public IEnumerable<LotDTO> GetList()
        {
            var ls = unitOfWork.lotsRepository.GetAll();
            var mapped = mapper.Map<IEnumerable<Lot>, IEnumerable<LotDTO>>(ls);
            return mapped;
        }


        /// <summary>Gets the list active lots</summary>
        /// <returns>LotDTOs list</returns>
        public IEnumerable<LotDTO> GetListActive()
        {
            var ls = unitOfWork.lotsRepository.GetAllActive();

            var mapped = mapper.Map<IEnumerable<Lot>, IEnumerable<LotDTO>>(ls);
            return mapped;
        }


        /// <summary>Gets the list of finished lots</summary>
        /// <returns>LotDTOs list</returns>
        public IEnumerable<LotDTO> GetListFinished()
        {
            var ls = unitOfWork.lotsRepository.GetAllFinished();

            var mapped = mapper.Map<IEnumerable<Lot>, IEnumerable<LotDTO>>(ls);
            return mapped;
        }


        /// <summary>Gets the list by category.</summary>
        /// <param name="category">The category.</param>
        /// <returns>Lot list</returns>
        /// <exception cref="Exception">Category string is empty</exception>
        public IEnumerable<LotDTO> GetListByCategory(string category)
        {
            if (category is null || category.Length == 0) throw new Exception("Category string is empty");

            var list = unitOfWork.lotsRepository.GetAllByCategory(category);

            var mapped = mapper.Map<IEnumerable<Lot>, IEnumerable<LotDTO>>(list);
            return mapped;
        }


        /// <summary>Gets the list where user play.</summary>
        /// <param name="mail">The mail.</param>
        /// <returns>Lot list</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public IEnumerable<LotDTO> GetListWhereUserPlay(string mail)
        {
            if (mail is null) throw new ArgumentNullException();

           var list= unitOfWork.lotsRepository.GetLotsWhereUserPlayed(mail);

            IEnumerable<LotDTO> mapped = mapper.Map<IEnumerable<Lot>, IEnumerable<LotDTO>>(list);
            return mapped;
        }


        /// <summary>Gets the users lot list</summary>
        /// <param name="login">The login.</param>
        /// <returns>Lot list</returns>
        public async Task<IEnumerable<LotDTO>> GetUsersListAsync(string login)
        {
            var list = await unitOfWork.lotsRepository.GetUsersLotsAsync(login);


            var mapped = mapper.Map<IEnumerable<Lot>, IEnumerable<LotDTO>>(list);
            return mapped;
        }


        /// <summary>Gets the lot.</summary>
        /// <param name="Id">The identifier.</param>
        /// <returns>LotDTO</returns>
        /// <exception cref="Exception">Lot with id { Id } does not exist</exception>
        public LotDTO GetLot(int Id)
        {
            var lot = unitOfWork.lotsRepository.GetById(Id);
            if (unitOfWork.lotsRepository.GetById(Id) is null) throw new Exception($"Lot with id { Id } does not exist");
            var mapped = mapper.Map<Lot, LotDTO>(lot);
            return mapped;
        }


        /// <summary>Starts the lot auction</summary>
        /// <param name="ID">The identifier.</param>
        /// <exception cref="Exception">Lot does not exist</exception>
        public void StartAuction(int ID)
        {

            var dblot = unitOfWork.lotsRepository.GetById(ID);

            if (dblot is null) throw new Exception("Lot does not exist");

            dblot.Status = LotStatus.Active;
            dblot.CreationTime = DateTime.Now;

           unitOfWork.lotsRepository.Update(dblot);
           unitOfWork.Save();
        }


        /// <summary>Updates the lot.</summary>
        /// <param name="lot">The lot.</param>
        /// <exception cref="Exception">Lot does not exist
        /// or
        /// Cannot update. Lot is in auction</exception>
        /// <exception cref="FieldAccessException">Wrong owner</exception>
        public void UpdateLot(LotDTO lot)
        {
            var mapped = mapper.Map<LotDTO, Lot>(lot);
            var login = Thread.CurrentPrincipal.Identity.Name;
            var status = unitOfWork.lotsRepository.GetSatus(mapped.Id);

            if (unitOfWork.lotsRepository.GetById(mapped.Id) == null) throw new Exception("Lot does not exist");
            if (!unitOfWork.lotsRepository.IsOwner(mapped.Id, login)) throw new FieldAccessException("Wrong owner");
            if (status != "NotApproved") throw new Exception("Cannot update. Lot is in auction");


            unitOfWork.lotsRepository.Update(mapped);
            unitOfWork.Save();
        }


        /// <summary>Admin updates lot.</summary>
        /// <param name="lot">The lot.</param>
        /// <exception cref="Exception">Bll,This lot does not exist
        /// or
        /// Cannot update. Lot is in auction</exception>
        public void AdminUpdateLot(LotDTO lot)
        {
            var mapped = mapper.Map<LotDTO, Lot>(lot);
            var status = unitOfWork.lotsRepository.GetSatus(mapped.Id);

            if (unitOfWork.lotsRepository.GetById(mapped.Id) == null) throw new Exception("Bll,This lot does not exist");
            if (status != "NotApproved") throw new Exception("Cannot update. Lot is in auction");


            unitOfWork.lotsRepository.Update(mapped);
            unitOfWork.Save();
        }


        /// <summary>Rejects the lot asynchronous.</summary>
        /// <param name="id">The identifier.</param>
        /// <exception cref="Exception">Lot does not exist</exception>
        public async Task RejectAsync(int id)
        {
            Lot lot = unitOfWork.lotsRepository.GetById(id);

            if (lot is null) throw new Exception("Lot does not exist");

            await unitOfWork.lotsRepository.RejectAsync(id);
            await unitOfWork.SaveAsync();
        }


        /// <summary>Check active lots for end. If time of lot ends, lot becomes "finished" and the winner is determined  </summary>
        /// <returns>Number of updated lots</returns>
        public async Task<int> CheckLotEndingAsync()
        {
            int counter = 0;
            IEnumerable<Lot> lots = unitOfWork.lotsRepository.GetAllActive();

            foreach(var a in lots)
            {
                DateTime buffer = ((DateTime)a.CreationTime).AddHours(a.Duration);
                if (buffer < DateTime.Now)
                {                  
                    //Finished status
                    a.Status = LotStatus.Finished;
                    //Select winner
                    if (a.Bets.Count != 0)
                    {
                        var betlist = unitOfWork.betsRepository.GetByLotId(a.Id);

                        Bet bet = betlist.OrderByDescending(p => p.Price).First();
                        User user = bet.Owner;
                        a.WinnerMail = user.Email;
                        //Return money to loosers
                        foreach(var i in betlist.Skip(1))
                        {
                           await unitOfWork.UserManager.AddMoney(i.Owner.Id, -i.Price);
                        }

                    }
                    //Counter
                    counter++;
                }
            }
            await unitOfWork.SaveAsync();
            return counter;
        }

    }
}
