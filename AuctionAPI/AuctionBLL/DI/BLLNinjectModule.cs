﻿using AuctionDAL;
using AuctionDAL.EF;
using AuctionDAL.Interfaces;
using Ninject.Modules;

namespace AuctionBLL.DI
{
    public class BLLNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>();
            Bind<MainContext>().To<MainContext>();   
        }
    }
}
