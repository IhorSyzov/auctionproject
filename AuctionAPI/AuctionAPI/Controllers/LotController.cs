﻿using AuctionAPI.Models;
using AuctionBLL.Interfaces;
using AuctionBLL.Models;
using AutoMapper;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace AuctionAPI.Controllers
{
    [RoutePrefix("api/lot")]
    [ExceptionsCheck]
    public class LotController : ApiController
    {
        readonly IMapper mapper;
        readonly ILotService service;

        public LotController(ILotService serv, IMapper map)
        {
            mapper = map;
            service = serv;
        }



        /// <summary>Gets the lot list.</summary>
        /// <returns>Lots list</returns>
        public IEnumerable<LotView> GetLotList()
        {
            IEnumerable<LotDTO> listFromService = service.GetList();

            IEnumerable<LotView> mappedList = mapper.Map<IEnumerable<LotDTO>, IEnumerable<LotView>>(listFromService);

            return mappedList;
        }


        /// <summary>Gets the lot by identifier.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Lot</returns>
        [Route("{id:int}")]
        public LotView GetLotById(int id)
        {
            LotDTO lot = service.GetLot(id);
            LotView mapped = mapper.Map<LotDTO, LotView>(lot);

            return mapped;
        }


        /// <summary>Adds new lot</summary>
        /// <param name="lot">The lot.</param>
        /// <returns>200/400 Status code</returns>
        [Authorize]
        public async Task<IHttpActionResult> PutNewLot(LotInput lot)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            LotDTO mapped = mapper.Map<LotInput, LotDTO>(lot);
            mapped.OwnerLogin = Thread.CurrentPrincipal.Identity.Name;

            LotDTO geted = await service.AddLotAsync(mapped);

            LotView answer = mapper.Map<LotDTO, LotView>(geted);
            return Created<LotView>($"api/lot/{answer.Id}", answer);
        }


        /// <summary>Deletes the lots that user own</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        ///   <para>200/400 Status Code</para>
        /// </returns>
        [Authorize]
        [Route("{id:int}")]
        public IHttpActionResult DeleteLot(int id)
        {
            string Login = Thread.CurrentPrincipal.Identity.Name;
            service.DeleteLot(id, Login);
            return Ok();
        }


        /// <summary>Gets the list of lots by category.</summary>
        /// <param name="name">The category</param>
        /// <returns>Lots list</returns>
        [Route("category/{name}")]
        public IEnumerable<LotView> GetByCategory(string name)
        {
            var listdto = service.GetListByCategory(name);

            var list = mapper.Map<IEnumerable<LotDTO>, IEnumerable<LotView>>(listdto);
            return list;
        }


        /// <summary>Gets the active lots.</summary>
        /// <returns>Lots list</returns>
        [Route("active")]
        public IEnumerable<LotView> GetActiveLots()
        {
            var listdto = service.GetListActive();

            var list = mapper.Map<IEnumerable<LotDTO>, IEnumerable<LotView>>(listdto);
            return list;
        }


        /// <summary>Gets the finished lots.</summary>
        /// <returns>Lots list</returns>
        [Route("finished")]
        public IEnumerable<LotView> GetFinishedLots()
        {
            var listdto = service.GetListFinished();

            var list = mapper.Map<IEnumerable<LotDTO>, IEnumerable<LotView>>(listdto);
            return list;
        }


        /// <summary>Gets the lots of current user</summary>
        /// <returns>lots list</returns>
        [Authorize]
        [Route("my")]
        public async Task<IEnumerable<LotView>> GetUsersLot()
        {
            string login = Thread.CurrentPrincipal.Identity.Name;

            var listdto = await service.GetUsersListAsync(login);

            var list = mapper.Map<IEnumerable<LotDTO>, IEnumerable<LotView>>(listdto);
            return list;
        }



        /// <summary>Updates a lot of current user</summary>
        /// <param name="inputlot">The inputlot.</param>
        /// <param name="id">The identifier.</param>
        [Authorize]
        [Route("{id:int}")]
        public IHttpActionResult PostUpdateLot(LotInput inputlot,int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            LotDTO lot = mapper.Map<LotInput, LotDTO>(inputlot);
            lot.Id = id;

            service.UpdateLot(lot);

            return Ok();
        }


        /// <summary>AdminFunction. Starts the lot.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>200/400 Status code</returns>
        [Authorize(Roles ="admin")]
        [Route("start/{id:int}")]
        public IHttpActionResult GetStartLot(int id)
        {
            service.StartAuction(id);

            return Ok();
        }


        /// <summary>Admin function. Rejects the lot</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>200/400 Status</returns>
        [Authorize(Roles = "admin")]
        [Route("admin/reject/{id:int}")]
        public async Task<IHttpActionResult> GetRejectLot(int id)
        {
            await service.RejectAsync(id);
            return Ok();
        }


        /// <summary>Gets the lot where user play.</summary>
        /// <returns>Lot list</returns>
        [Authorize]
        [Route("play")]
        public IEnumerable<LotView> GetLotWhereUserPlay()
        {
            string login = Thread.CurrentPrincipal.Identity.Name;

            IEnumerable<LotDTO> dtolist=service.GetListWhereUserPlay(login);

            IEnumerable<LotView> list = mapper.Map<IEnumerable<LotDTO>, IEnumerable<LotView>>(dtolist);
            return list;
        }


        /// <summary>Admin function. Deletes the user.</summary>
        /// <param name="id">The identifier.</param>
        /// <returns>200/400 STatus</returns>
        [Authorize(Roles ="admin")]
        [Route("admin/{id:int}")]
        public IHttpActionResult DeleteUser(int id)
        {
            service.HardDelete(id);
            return Ok();
        }


        /// <summary>Admin function. Updates the lot</summary>
        /// <param name="inputlot">The inputlot.</param>
        /// <param name="id">The identifier.</param>
        /// <returns>200/400 Status</returns>
        [Authorize(Roles = "admin")]
        [Route("admin/{id:int}")]
        public IHttpActionResult PostAdminUpdateLot(LotInput inputlot, int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            LotDTO lot = mapper.Map<LotInput, LotDTO>(inputlot);
            lot.Id = id;

            service.UpdateLot(lot);
            return Ok();
        }
    }
}
