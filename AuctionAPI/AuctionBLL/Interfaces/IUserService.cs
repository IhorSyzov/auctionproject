﻿
using AuctionBLL.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AuctionBLL.Interfaces
{
    public interface IUserService:IDisposable
    {       
        IEnumerable<UserDTO> GetUsers();
        Task CreateAsync(UserDTO userDto);
        Task<UserDTO> GetUserByMailAsync(string mail);
        Task<UserDTO> FindUserAsync(string login, string pass);
        Task<UserDTO> FindUserAsync(string ID);
        Task DeleteAsync(string id);
        Task DeleteMyAccountAsync();
        Task ChangeMyPassAsync(string oldpass, string newpass);
        Task UpdateUserBioAsync(UserDTO user);      
        Task SetInitialDataAsync(UserDTO adminDto, List<string> roles);
        Task AddRoleAsync(string userId, string role);
        Task<ClaimsIdentity> GenerateIdentityAsync(string login, string pass, string authenticationType);
    }
}
